const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const client = require('twilio')('AC080b418fd30af3743206f724c2f221ac', '949416a2c40d7a59d95e388adc2cc441');
const cron = require("node-cron");

var FCM = require('fcm-node');
var serverKey = 'AAAAtWaDZdI:APA91bF3Hxj2E-rjVv5BHrpAqJ_bezUXR9Mkd8s7y3lbtxaKeAc6ZeJpPRhgKcguMZv0MLJxdPkt2o8u9HXxScIP6qnhjqCU5KETFF5dTv-cgAYrWrKtiLVwwXvvyixxuhu27Wb9FWpu';
    
var multer = require('multer');

// parse application/json
app.use(bodyParser.json());

app.use(express.static(__dirname+ '/public'));

//create database connection
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'mbh@aman@123',
    database: 'mbh',
    multipleStatements: true
});

var storage = multer.diskStorage({
    destination: function (request, file, callback) {
        callback(null, __dirname + '/public/images');
    },
    filename: function (request, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
    }
});

var upload = multer({ storage: storage });

//connect to database
conn.connect();

//checking if result empty
function isRes(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify([]);
}

//checking if body empty 
function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify({});
}

//generate random digits
function rand(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

// Verify Token
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['accesstoken'];
    if (req.headers['accesstoken']) {
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader;
            if (bearer) {
                const bearerToken = bearer;
                req.token = bearerToken;
                next();
            } else {
                res.sendStatus(401);
            }
        } else {
            res.sendStatus(401);
        }
    } else {
        res.sendStatus(401);
    }
}

function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}


function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];

        if (typeof x == "string") {
            x = ("" + x).toLowerCase();
        }
        if (typeof y == "string") {
            y = ("" + y).toLowerCase();
        }

        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

//send email
var transporter = nodemailer.createTransport({
    service: 'gmail',
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user: 'androidmaster358@gmail.com',
        pass: 'bogbybtzxwadquyk'
    }
});

var fcm = new FCM(serverKey);

function notification(data) {

    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: data.to, 
        collapse_key: 'First notification',
        
        notification: {
            title: data.title, 
            body: data.body 
        },
        
        data: {  //you can send only notification or only data(or include both)
            my_key: data.title,
            my_another_key: data.body,
            click_action:'FLUTTER_NOTIFICATION_CLICK'
        }
    };
    
    fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!");
            } else {
                console.log("Successfully sent with response: ", response);
            }
    }); 
}

function ios_notification(data) {

	const message = {
	   registration_ids: data.to,

	    notification: {
	      title: data.title,
	      body:data.body,
	      "vibrate": 1,
	      "sound": 1,
	      "show_in_foreground": true,
	      "priority": "high",
	      "content_available": true,
	    },
	    data: {
	      title: data.title,
	      body: data.body,
	      click_action:'FLUTTER_NOTIFICATION_CLICK'
	    }
	}
}
 

// Check api's working fine
app.get('/api', (req, res) => {
    res.json({
        message: 'The blood hero all apis working fine'
    });
});

/*check user in db*/
app.post('/api/social_login', (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        let sql = 'SELECT * FROM user where social_id="' + req.body.social_id + '" and  provider="' + req.body.provider + '"';
        let query = conn.query(sql, (err, result) => {
            if (!isRes(result)) {
                for (let key in result) {
                    let data = result[key];
                    jwt.sign({
                        data
                    }, 'accesstoken', {
                        expiresIn: '1440h'
                    }, (err, token) => {
                        res.json({
                            "status": 200,
                            token,
                            "message": "You are logged in now..",
                            "data": data
                        });
                    })
                }

            } else {
                res.send({
                    "status": 402,
                    "error": "user not found in our records"
                });
            }
        })
    }
});

// add new User (Sign up)
app.post('/api/signup', (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 204,
            "message": "Please provide all parameters"
        });
    } else {
        let sql = 'SELECT * FROM user where email="' + req.body.email + '" or mobile="' + req.body.mobile + '" limit 1';
        let query = conn.query(sql, (err, result) => {
            if (isRes(result)) {
                let data = {
                    user_id: rand(100000, 999999),
                    fname: req.body.fname,
                    lname: req.body.lname,
                    mobile: req.body.mobile,
                    email: req.body.email,
                    dob: req.body.dob,
                    blood_type: req.body.blood_type,
                    medical_attr_id: req.body.medical_attr_id,
                    gender: req.body.gender,
                    addressOne: req.body.addressOne,
                    addressOneLatitude: req.body.addressOneLatitude,
                    addressOneLongitude: req.body.addressOneLongitude,
                    addressTwo: req.body.addressTwo,
                    addressTwoLatitude: req.body.addressTwoLatitude,
                    addressTwoLongitude: req.body.addressTwoLongitude,
                    social_id: req.body.social_id,
                    provider: req.body.provider,
                    is_donor: req.body.is_donor,
                    is_covid19: req.body.is_covid19,
                    profile_image: "https://i.pinimg.com/564x/04/56/86/0456869906dfa906c494b2b63aa67f2a.jpg"
                };
                let sql = "INSERT INTO user SET ?";
                let query = conn.query(sql, data, (err, result) => {
                    if (err) {
                        res.json({
                            "status": 401,
                            "error": err
                        });
                    } else {
                        jwt.sign({
                            data
                        }, 'accesstoken', {
                            expiresIn: '1440h'
                        }, (err, token) => {
                            res.json({
                                "status": 200,
                                token,
                                "message": "Sign up Successful.",
                                "profile": data
                            });
                            var mailOptions = {
                                from: 'androidmaster358@gmail.com',
                                to: req.body.email,
                                subject: 'Signup Successful',
                                text: 'Your account is successfully registered in MyBloodHero.'
                            };
                            transporter.sendMail(mailOptions);
                        });
                    }
                });
            } else {
                if (result[0].mobile == req.body.mobile) {
                    res.json({
                        "status": 401,
                        "error": "Please use another mobile, This mobile is already Existed.."
                    });
                } else if (result[0].email == req.body.email) {
                    res.send({
                        "status": 401,
                        "error": "Please use another email, This email is already Existed.."
                    });
                } else {
                    res.send({
                        "status": 401,
                        "error": "Parameters fine but something else went wrong"
                    });
                }
            }
        });
    }
});

//generating otp 
app.post('/api/generateOtp', (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        let sql = 'SELECT * FROM otp WHERE mobile="' + req.body.mobile + '" and expire="0" and created_at > now() - interval 1 minute ORDER BY id DESC limit 1';
        let query = conn.query(sql, (err, results) => {
            if (!isRes(results)) {
                res.send({
                    "status": 401,
                    "error": "wait for sometimes"
                });
            } else {
                let update = 'UPDATE otp SET expire=1 WHERE mobile="' + req.body.mobile + '" and expire="0"';
                let querys = conn.query(update);

                var otp = rand(100000, 999999);
                let data = {
                    otp: otp,
                    mobile: req.body.mobile,
                    expire: 0
                };
                let insert = "INSERT INTO otp SET ?";
                let query = conn.query(insert, data)
                client.messages
                    .create({
                        body: 'This is your OTP: ' + otp + '. Please do not share this OTP to anyone.',
                        from: '+12569989471',
                        to: '+91"' + req.body.mobile + '"'
                    })
                    .then(message => console.log(message.sid));
                res.send({
                    "status": 200,
                    "message": "OTP sent successfully Please Confirm your OTP now..."
                });
            }
        });
    }
});

// verification otp 
app.post('/api/otpConfirm', (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        let sql = 'SELECT * FROM otp WHERE mobile="' + req.body.mobile + '" ORDER BY id DESC limit 1';
        let query = conn.query(sql, (err, result) => {
            if (result[0].expire == 0) {
                if (req.body.otp == result[0].otp) {
                    let sqlu = 'UPDATE otp SET expire=1 WHERE mobile=' + req.body.mobile;
                    let queryu = conn.query(sqlu);
                    let sql = 'SELECT * FROM user WHERE mobile=' + req.body.mobile;
                    let query = conn.query(sql, (err, results) => {
                        if (isRes(results)) {
                            res.send({
                                "status": 200,
                                "message": "Otp verification Successfully. Please Sign up first now..."
                            });
                        } else {
                            for (let key in results) {
                                let data = results[key];
                                jwt.sign({
                                    data
                                }, 'accesstoken', {
                                    expiresIn: '1440h'
                                }, (err, token) => {
                                    res.json({
                                        "status": 200,
                                        token,
                                        "message": "Otp verification Successfully. You are logged in now..",
                                        "data": data
                                    });
                                });
                            }
                        }
                    });
                } else {
                    res.send({
                        "status": 401,
                        "error": "The otp you provided is wrong"
                    });
                }
            } else {
                res.send({
                    "status": 401,
                    "error": "OTP Expired. Please try again."
                }); //Already Used
            }
        });
    }
});

// Creating Request
app.post('/api/createreq', verifyToken, (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        var verify = jwt.verify(req.token, 'accesstoken');
        let data = {
            user_id: verify.data.user_id,
            request_id: rand(100000, 999999),
            typeofdonation: req.body.typeofdonation,
            bloodtype: req.body.bloodtype,
            bloodrequestfor: req.body.bloodrequestfor,
            numberofunits: req.body.numberofunits,
            addressofdonation: req.body.addressofdonation,
            donationlatitude: req.body.donationlatitude,
            donationlongitude: req.body.donationlongitude,
            needbydatetime: req.body.needbydatetime,
            mobile: verify.data.mobile,
            alternatenumber: req.body.alternatenumber,
            isnewsfeedvisible: req.body.isnewsfeedvisible
        };
        let sql = "INSERT INTO request SET ?";
        let query = conn.query(sql, data, (err, result) => {
            if (err) {
                res.send({
                    "status": 401,
                    "error": err
                });
            } else {
                res.send({
                    "status": 200,
                    "message": "Request Created Successfully"
                });
            }
        });
    }
});

//nearbuy requests
app.post('/api/nearbydonation', verifyToken, (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        var verify = jwt.verify(req.token, 'accesstoken');
        var fil = req.body.filter;
        if (fil == null) {
            var filter = 4;
        } else {
            var filter = fil;
        }

        var lat = req.body.lat;
        var lng = req.body.lng;
        var len = filter.length;
        if (len > 1) {
            var z = filter.split(',');
            var y = z.length;
            if (y == 3) {
                if (req.body.lat && req.body.lng) {
                    var n = new Date().getMonth() + 1;
                    var action = 'and MONTH(needbydatetime)=' + n + '  and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday") ORDER BY dist ASC';
                    var sort = 'dist';
                } else {
                    var n = new Date().getMonth() + 1;
                    var action = 'and MONTH(needbydatetime)=' + n + '  and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday") ORDER BY dist ASC';
                    var sort = 'dist';
                    var lat = verify.data.addressOneLatitude;
                    var lng = verify.data.addressOneLongitude;
                }
            } else {
                if (z[0] == 0 && z[1] == 1 || z[0] == 1 && z[1] == 0) {
                    var n = new Date().getMonth() + 1;
                    var action = "and MONTH(needbydatetime)=" + n + "  ORDER BY dist ASC";
                    var sort = 'dist';
                } else if (z[0] == 0 && z[1] == 2 || z[0] == 2 && z[1] == 0) {
                    var action = 'and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday") ORDER BY dist ASC';
                    var sort = 'dist';
                } else {
                    var n = new Date().getMonth() + 1;
                    var action = 'and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday") and MONTH(needbydatetime)=' + n + ' ORDER BY needbydatetime ASC';
                    var sort = 'needbydatetime';
                }
            }
        } else {
            if (filter == 0) {
                var action = "ORDER BY dist ASC";
                var sort = 'dist';
            } else if (filter == 1) {
                var n = new Date().getMonth() + 1;
                var action = "and MONTH(needbydatetime)=" + n + " ORDER BY needbydatetime ASC";
                var sort = 'needbydatetime';
            } else if (filter == 2) {
                if (req.body.lat && req.body.lng) {
                    var action = 'and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday")';
                    var sort = 'needbydatetime';
                } else {
                    var action = 'and (DAYNAME( DATE(`needbydatetime` ) )="sunday" or DAYNAME( DATE(`needbydatetime` ) )="saturday")';
                    var lat = verify.data.addressOneLatitude;
                    var lng = verify.data.addressOneLongitude;
                    var sort = 'needbydatetime';
                }
            } else if (filter == 3) {
                    var action = "and bloodtype='" + verify.data.blood_type + "'";
                    var sort = 'needbydatetime';
            } else {
                var action = 'ORDER BY needbydatetime ASC';
                var sort = 'needbydatetime';
            }
        }
            let sql = "SELECT r.*, U.profile_image,U.fname,U.lname,R.status, R.donor_id , floor(SQRT(POW(69.1 * (donationlatitude - " + lat + "), 2) +POW(69.1 * (" + lng + " - donationlongitude) * COS(donationlatitude / 57.3), 2))*1.609) AS dist FROM request as r LEFT JOIN user as U ON r.user_id=U.user_id LEFT JOIN req_status as R ON r.request_id=R.request_id and R.donor_id=" + verify.data.user_id + " where needbydatetime > now()  and r.confirmed < r.numberofunits and r.user_id!=" + verify.data.user_id + "  HAVING dist < 30 " + action + " ";
            let query = conn.query(sql, (err, result) => {
                if (isRes(result)) {
                    res.json({
                        "status": 401,
                        "message": 'No Result Found'
                    });
                } else {
                    res.send({
                        "status": 200,
                        "result": result
                    });
                }
        });
    }
});

//get organisation list
app.get('/api/getorg', verifyToken, (req, res) => {
    let sql = 'SELECT * FROM get_org';
    let query = conn.query(sql, (err, result) => {
        res.send({
            "status": 200,
            result
        });
    });
});


function get_token(id, callback) {
    var query = conn.query('SELECT token FROM user where user_id= ? LIMIT 1',[id],function(err, result) {           
        callback(null, result);            
    });
}

function request_ck(req,id, callback) {
    var query = conn.query('SELECT * FROM req_status where request_id=? and donor_id = ?' ,[req,id],function(err, result) {           
        callback(null, result);            
    });
}
function update_ck(data,req,id,callback) {
    var query = conn.query('UPDATE req_status SET ? where request_id=? and donor_id = ?', [data,req,id], function(err, result) {          
        callback(null, result);            
    });
}
//user request status
app.post('/api/acceptdeclinereq', verifyToken, (req, res) => {
    if (isEmpty(req.body)) 
    {
        res.send({"status": 400,"message": "Please provide all parameters"});
    } 
    else 
    {
        var status=req.body.status;
        var verify = jwt.verify(req.token, 'accesstoken');
        request_exist(req.body.request_id,function(err, result) { 
            if (isRes(result)) 
            {
                res.send({"status": 402,"message": "Request_id does not exist in our records"});
            } 
            else 
            {
                if(status==1)
                {
                    eligible(verify.data.user_id, function(err, resul) {
                        if (isRes(resul)) 
                        {
                        	request_ck(req.body.request_id,verify.data.user_id,function(err, reqdata) {
                        		if(isRes(reqdata))
                        		{
                                    var data={user_id:result[0].user_id,status:status,request_id:req.body.request_id,donor_id:verify.data.user_id};
		                            insert('req_status',data, function(err, datas) {
		                                if(err){res.json({"status":401,"message":err});}
		                                else
		                                {
		                                	if(verify.data.is_donor==0) {
				                            	update('user',{is_donor:1},{user_id:verify.data.user_id},function(err, reqdata) {});
				                            }

				                            get_token(result[0].user_id,function(err, result) { 
												if(result[0].token===null){console.log('Token not found');}
												else 
												{
													var data={to:result[0].token,"title":"Request accepted","body":verify.data.fname+' '+verify.data.lname+ " has accepted your blood request.."};
					                                notification(data);
												}
											});
		                                	res.json({"status":200,"message":"Successfully saved data"});
		                                }
		                            });
                        		}
                        		else
                        		{
                        			res.json({"status":402,"message":"You can't accept this request right now"});
	                            }
                        	}); 
                        } 
                        else 
                        {
                            res.json({"status": 200,"message": "This user is not eligible for donation"});
                        }
                    })
                }
                else if(status==2)
                {
                    request_ck(req.body.request_id,verify.data.user_id,function(err, reqdata) {
                    	if(!isRes(reqdata))
                    	{
                    	if(reqdata[0].status==3)
                    	{
                            var data={status:status,reason:req.body.reason};
	                        update_ck(data,req.body.request_id,verify.data.user_id, function(err, udata) {
	                            if(err){res.json({"status":401,"message":err})}
	                            else{
	                            	get_token(reqdata[0].user_id,function(err, results) { 
										if(results[0].token===null){console.log('Token not found');}
										else 
										{
											var data={to:results[0].token,"title":"Request declined","body":verify.data.fname+' '+verify.data.lname+ " has declined his participation in your blood request."};
					                        notification(data);
					                        console.log(data);
										}
								    });
	                            	res.json({"status":200,"message":"Successfully Updated data"});
	                            }
	                        });
                    	}
                    	else
                    	{
	                        var data={status:status,reason:req.body.reason};
	                        update_ck(data,req.body.request_id,verify.data.user_id, function(err, udata) {
	                            if(err){res.json({"status":401,"message":err})}
	                            else{res.json({"status":200,"message":"Successfully Updated data"});}
	                        });
                        }
                    }
                    else
                    {
                        res.json({"status":401,"message":"Request id not exists in the database."});	
                    }
                });
                }
                else
                {
                    res.json({"status":401,"message":"wrong status code"});
                }
            }
        })
    }
});

function request_exist(id, callback) {
    var query = conn.query('SELECT * FROM request where request_id=' + id + '',function(err, resul) {           
        callback(null, resul);            
    });
}

function confirmed(id, callback) {
    var query = conn.query('SELECT count(*) as confirmed FROM req_status where request_id=' + id + ' and (status=3 or status=5)',function(err, result) {           
        callback(null, result);            
    });
}

function update_req_status(data,id,req, callback) {
    var query = conn.query('UPDATE req_status SET ? WHERE donor_id = ? and request_id = ?', [data,id,req], function(err, result) {          
        callback(null, result);            
    });
}

function update(tbl,data,id,callback) {
    var query = conn.query('UPDATE ?? SET ? WHERE ?', [tbl,data,id], function(err, result) {          
        callback(null, result);            
    });
}

//user request status
app.post('/api/acceptedbyrequestor', verifyToken, (req, res) => {
    if (isEmpty(req.body)) 
    {
        res.send({"status": 400,"message": "Please provide all parameters"});
    } 
    else 
    {
        var verify = jwt.verify(req.token, 'accesstoken');
        request_exist(req.body.request_id, function(err, exist) 
        {
            if(!isRes(exist))
            {
                if(req.body.status == 3)
                {
                    confirmed(req.body.request_id, function(err, con) 
                    {
                        if(con[0].confirmed<exist[0].numberofunits)
                        {
                           eligible(req.body.donor_id, function(err, result) 
                            {
                                if (isRes(result)) 
                                {
                                    let data = {status:req.body.status,reason:req.body.reason};
                                    update_req_status(data,req.body.donor_id,req.body.request_id, function(err, datas) {
                                        if(err){res.json({"status":401,"message":err});}
                                        else{res.json({"status":200,"message":"Data updated successfully"});}
                                    });
                                } 
                                else 
                                {
                                   res.json({"status": 200,"message": "This user is not eligible for donation"});
                                }
                            });
                        }
                        else
                        {
                            res.json({"status": 401,"message": "Confirmed user exceed"});
                        }
                    });
                }
                else if(req.body.status==4)
                {
                	let data = {status:req.body.status,reason:req.body.reason};
                        update_req_status(data,req.body.donor_id,req.body.request_id, function(err, datas) {
                            if(err){res.json({"status":401,"message":err});}
                            else {
	                            	res.json({"status":200,"message":"Data updated successfully"});
		                            get_token(req.body.donor_id,function(err, result) { 
										if(result[0].token===null) {console.log('Token not found');}
										else 
										{
		                            	    var data={to:result[0].token,title:"Request declined",body:verify.data.fname+' '+verify.data.lname+ " has declined your participation"};
				                            notification(data);
				                            console.log(data);
		                                }
		                            });
                                }
                            });
                }
                else if(req.body.status==5)
                {
                    var data={status:req.body.status,reason:req.body.reason};
                    update_req_status(data,req.body.donor_id,req.body.request_id, function(err, datas) {
                        if(err){res.json({"status":401,"message":err});}
                        else {
                        	insert('donation_count',data, function(err, datas){});
		                    get_token(data.user_id,function(err, result) { 
								if(result[0].token===null) {console.log('Token not found');}
								else {
									var data={to:result[0].token,title:"Donation completed",body:"Thanks for completing your blood donation"};
		                            notification(data);
								}
							});
                            res.json({"status":200,"message":"Data updated successfully"});
                        }
                    });  
                }
                else
                {
                	res.json({"status":401,"message":"Wrong status code"});
                }

                confirmed(req.body.request_id, function(err, cons) {
                	    console.log(cons);
                        update('request',{"confirmed":cons[0].confirmed},{request_id:req.body.request_id}, function(err, datas) {});
                });
            }
            else
            {
                res.json({"status":404,"message":"Request_id not found in database"});
            }
        });    
    }
});


//get single request details
app.post('/api/getreqdetails', verifyToken, (req, res) => {
    if (isEmpty(req.body)) {
        res.send({
            "status": 400,
            "message": "Please provide all parameters"
        });
    } else {
        let sqlu = 'SELECT * FROM request where request_id=' + req.body.request_id;
        let query = conn.query(sqlu, (err, data) => {
            if (isRes(data)) {
                res.send({
                    "status": 402,
                    "message": "Request_id does not exist in our records"
                });
            } else {
                var verify = jwt.verify(req.token, 'accesstoken');
                let sql = 'SELECT request.*,user.fname,user.lname FROM request LEFT JOIN req_status ON request.request_id=req_status.request_id LEFT JOIN user ON request.user_id=user.user_id where request.request_id=' + req.body.request_id; //specific request details
                let query = conn.query(sql, (err, result) => {
                    if (isRes(result)) {
                        res.send({
                            "status": 401,
                            "message": "No result found"
                        });
                    } else {

                        if (verify.data.user_id == result[0].user_id) //if created by me
                        {
                            let sqls = 'SELECT R.user_id,U.user_id,U.fname as accepter_fname,U.lname as accepter_lname,U.mobile,R.status,0 as "ratings",count(D.id) as donation_count,U.profile_image FROM req_status as R LEFT JOIN user as U ON R.donor_id=U.user_id LEFT JOIN donation_count as D ON R.donor_id=D.user_id  where R.request_id=' + req.body.request_id + ' and  R.status!=2 and R.status!=4 GROUP BY R.user_id,U.user_id,D.user_id,U.fname,U.lname,U.mobile,R.status,U.profile_image';
                            let querys = conn.query(sqls, (err, accepter_data) => {
                                for (let key in result) {
                                    let value = result[key];
                                    res.json({
                                        "status": 200,
                                        "result": value,
                                        "accepter_data":accepter_data
                                    });
                                }
                            })
                        } else {
                            let sqls = 'SELECT status FROM req_status where donor_id=' + verify.data.user_id + ' and req_status.request_id=' + req.body.request_id + ' ORDER by created_at desc  '; //created by others
                            let querys = conn.query(sqls, (err, data) => {
                                if (isRes(data)) {
                                    for (let key in result) {

                                        let value = result[key];
                                        let mystatus = null;
                                        res.json({
                                            "status": 200,
                                            "result": value,
                                            mystatus
                                        });
                                    }
                                } else {
                                    for (let key in result) {
                                        let value = result[key];
                                        for (let key in data) {
                                            let mystatus = data[key];
                                            res.json({
                                                "status": 200,
                                                "result": value,
                                                mystatus
                                            });
                                        }
                                    }
                                }
                            })
                        }
                    }
                });
            }
        });
    }
});

//get user count records
app.get('/api/getrequestdonorcount', verifyToken, (req, res) => {
    let sql = 'Select (select count(id) from user where is_donor=1) as donor_count, (select count(id) from request where needbydatetime>now()) as request_count';
    let query = conn.query(sql, (err, result) => {
        for (let key in result) {
            let value = result[key];
            res.json({
                "status": 200,
                "result": value
            });
        }
    });
});

function eligible(id, callback) {
    var query = conn.query('SELECT r.*,R.* FROM request as r LEFT JOIN req_status as R ON r.request_id=R.request_id  where (r.needbydatetime > now()-interval 2 month and r.needbydatetime < now() and R.status=5 and R.donor_id=' + id + ') or (r.needbydatetime > now()  and  r.needbydatetime < now()+interval 2 month and R.status=3 and R.donor_id=' + id + ')',function(err, result) {           
        callback(null, result);            
    });
}


//get user count records
app.get('/api/isEligiiblefordonation', verifyToken, (req, res) => {
    var verify = jwt.verify(req.token, 'accesstoken');

    eligible(verify.data.user_id, function(err, result) {
        if (isRes(result)) 
        {
            res.json({"status": 200,"result": true});
        } 
        else 
        {
            res.json({"status": 200,"result": false});
        }
    });
});


function already (id,d_id,req, callback) {
    var query = conn.query('select * from feedback where user_id= ? and donor_id=? and request_id=?',[id,d_id,req], function(err, result) {           
        callback(null, result);            
    });
}

function avg (id,callback) {
    var query = conn.query('select AVG(rating) as avg_rating from feedback where donor_id=?',[id], function(err, result) {           
        callback(null, result);            
    });
}

function update_feedback(data,id,d_id,req,callback) {
    var query = conn.query('UPDATE feedback SET ? WHERE user_id= ? and donor_id=? and request_id=?', [data,id,d_id,req], function(err, result) {          
        callback(null, result);            
    });
}
function get_status (id,d_id,req,callback) {
    var query = conn.query('select status,updated_at,now() as da from req_status where user_id= ? and donor_id=? and request_id=? and status=5',[id,d_id,req], function(err, result) {           
        callback(null, result);            
    });
}


//user request status
app.post('/api/feedback', verifyToken, (req, res) => {
    if (isEmpty(req.body)) {
        res.send({"status": 400,"message": "Please provide all parameters"});
    } 
    else {
        var verify = jwt.verify(req.token, 'accesstoken');
        var data = {user_id: verify.data.user_id,donor_id:req.body.donor_id,request_id: req.body.request_id,feedback: req.body.feedback,rating:req.body.rating};
        
        get_status(data.user_id,data.donor_id,data.request_id, function(err, status) {
            if(isRes(status))
            {
                res.send({"status":401,"message":"Update donor status to 5 before giving feedback"});
            }
            else
            {
                var updated=status[0].updated_at;
                var add_five_days = new Date(updated.getTime()+(5*24*60*60*1000));
                if(add_five_days < status[0].da)
                {
                  res.send({"status":401,"message":"You can't give feedback now"});
                }
                else
                {
                    already(verify.data.user_id,req.body.donor_id,req.body.request_id, function(err, ex) {
                        if(isRes(ex))
                        {
                            insert('feedback',data, function(err, data){
                                res.send({"status": 200,"message": "Data successfully saved"});
                            });
                            avg(req.body.donor_id, function(err, found) {
                            	if(isRes(found))
                            	{
                                   res.json({"status":402,"message":"Something went wrong"});
                            	}
                            	else
                            	{ 
                            		var info={is_rating_done:1,rating:found[0].avg_rating};
                                    update_req_s(info,verify.data.user_id,req.body.request_id,req.body.donor_id, function(err, datas) {}); 
                            	}
                            });
                        }
                        else
                        {
                            res.send({"status":200,"message":"Already given feedback"});
                        }
                    });
                }
            }
        });
    }
});

function getdonation(id, callback) {
    var query = conn.query('SELECT r.*,U.fname,U.lname,R.status ,U.profile_image FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id LEFT JOIN user as U ON R.user_id=U.user_id where needbydatetime < now() and R.donor_id=' + id + ' and R.status=5',function(err, result) {           
        callback(null, result);            
    });
}

function getdonationup(id, callback) {
    var query = conn.query('SELECT R.*,"others" as "created_by",U.fname,U.lname,r.status,U.profile_image FROM req_status as r LEFT JOIN request as R ON r.request_id=R.request_id LEFT JOIN user as U ON r.user_id=U.user_id where r.user_id!=' + id + ' and R.needbydatetime > now() and r.donor_id=' + id + ' and (r.status=3 or r.status=1)',function(err, result) {           
        callback(null, result);            
    });
}


//get user count records
app.get('/api/getmydonation', verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');

        var data=[];
    	getdonation(verify.data.user_id, function(err, result) {
        	if(isRes(result))
        	{
               data.push(); 
        	}
        	else
        	{
               for (let key in result) 
               {
                let api = result[key];
                data.push(api);
               }
        	}

        var zz=[];
    	getdonationup(verify.data.user_id, function(err, result) {
        	if(isRes(result))
        	{
               zz.push(); 
        	}
        	else
        	{
        		for (let key in result) 
               {
                let data = result[key];
                zz.push(data);
               }
               
        	}

		        setTimeout(function() {
		    	if(isRes(data))
		    	{
		    		var past=({"status":400,"message":"No data found"});
		    	}
		    	else
		    	{
		    		var past=({"status":200,"result":data});
		    	}
		    	if(isRes(zz))
		    	{
		    		var up=({"status":400,"message":"No data found"});
		    	}
		    	else
		    	{
		    		var up=({"status":200,"result":zz});
		    	}

		    	res.json({"past":past,"upcoming":up});
		    }, 30);
		});
   });
});


function getrequest(id, callback) {
    var query = conn.query('SELECT *,null as "accepter_data" FROM request where user_id = '+id+' and needbydatetime < now()',function(err, result) {           
        callback(null, result);            
    });
}
function getrequest_up(id, callback) {
    var query = conn.query('SELECT *,null as "accepter_data" FROM request where user_id = '+id+' and needbydatetime > now()',function(err, result) {           
        callback(null, result);            
    });
}

function getpastreq(id,callback) {
    var query = conn.query('SELECT r.* FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id where R.request_id=' + conn.escape(id)+ ' and needbydatetime < now() GROUP BY id', function(err, pastreq) {           
        callback(null, pastreq);            
    });
}

function getupreq(id,callback) {
    var query = conn.query('SELECT r.* FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id where R.request_id=' + conn.escape(id)+ ' and needbydatetime > now() GROUP BY id', function(err, upreq) {           
        callback(null, upreq);            
    });
}


//get user count records
app.get('/api/getmyrequest', verifyToken, (req, res) => {
    var verify = jwt.verify(req.token, 'accesstoken');

    var data=[];
    getrequest(verify.data.user_id, function(err, result) {
	    	if(isRes(result)) 
	    	{
	            data.push();
	    	} 
	    	else 
	    	{
	    		result.forEach(function(past) {
		    		getpastreq(past.request_id, function(err, pastreq) {
					    if(isRes(pastreq))
					    {
	                       data.push(past)
					    }
					    else
					    {
				            pastreq.forEach(function(result) { 
				                var sqll = 'SELECT R.user_id,U.user_id,U.fname as accepter_fname,U.lname as accepter_lname,U.mobile,R.status,0 as "ratings",count(D.id) as donation_count,U.profile_image FROM req_status as R LEFT JOIN user as U ON R.donor_id=U.user_id LEFT JOIN donation_count as D ON R.donor_id=D.user_id  where R.request_id=' + result.request_id + '  and  R.status!=2 and R.status!=4 GROUP BY  R.user_id,U.user_id,D.user_id,U.fname,U.lname,U.mobile,R.status,U.profile_image';
				                let queryl = conn.query(sqll, (err, accepter_data) => {
				                    data.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": accepter_data});
				                })
				            });
					    }
		    		});
	    		});
	    	}
        })
    var zz=[];
    getrequest_up(verify.data.user_id, function(err, results) {
	    	if(isRes(results)) 
	    	{
	            zz.push();
	    	} 
	    	else 
	    	{
	    		results.forEach(function(pasts) {
		    		getupreq(pasts.request_id, function(err, pastreq) {

					    if(isRes(pastreq))
					    {
	                       zz.push(pasts)
					    }
					    else
					    {
				            pastreq.forEach(function(result) { 
				                var sqll = 'SELECT R.user_id,U.user_id,U.fname as accepter_fname,U.lname as accepter_lname,U.mobile,R.status,0 as "ratings",count(D.id) as donation_count,U.profile_image FROM req_status as R LEFT JOIN user as U ON R.donor_id=U.user_id LEFT JOIN donation_count as D ON R.donor_id=D.user_id  where R.request_id=' + result.request_id + '  and  R.status!=2 and R.status!=4 GROUP BY  R.user_id,U.user_id,D.user_id,U.fname,U.lname,U.mobile,R.status,U.profile_image';
				                let queryl = conn.query(sqll, (err, accepter_data) => {
				                    zz.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": accepter_data});
				                })
				            });
					    }
		    		});
	    		});
	    	}
        })

    setTimeout(function() {
    	if(isRes(data))
    	{
    		var past=({"status":400,"message":"No data found"});
    	}
    	else
    	{
    		var past=({"status":200,"result":data});
    	}
    	if(isRes(zz))
    	{
    		var up=({"status":400,"message":"No data found"});
    	}
    	else
    	{
    		var up=({"status":200,"result":zz});
    	}

    	res.json({"past":past,"upcoming":up});
    }, 30);
});

function getprofile (id, callback) {
    var query = conn.query('SELECT user.*,(SELECT COUNT(*) FROM request WHERE user_id=?) as req_count,(SELECT COUNT(*) FROM donation_count WHERE user_id=?) as don_count FROM user WHERE user.user_id=?',[id,id,id], function(err, result) {           
        callback(null, result);            
    });
}
/*function count (id, callback) {
    var query = conn.query('select user.*,count(request.id) as request_count from user  LEFT JOIN request ON user.user_id=request.user_id  where user.user_id = ? GROUP BY user.id',[id], function(err, result) {           
        callback(null,result);            
    });
}*/

//get user count records
app.get('/api/getprofile', verifyToken, (req, res) => {
    var verify = jwt.verify(req.token, 'accesstoken');
    var id=verify.data.user_id;
    getprofile(id, function(err, data) {
        if(isRes(data))
        {
            res.json({"status":404,"message":"No data found"});
        }
        else
        {
            for (let key in data) 
            {
                let a = data[key];
                res.json({"status":200,"result":a});
            } 
        }
    })
});

this.updateprofile = function(data,id, callback) {
    var query = conn.query('UPDATE user SET ? WHERE ?', [data,id], function(err, result) {          
        callback(null, result);            
    });
}

//get user count records
app.post('/api/updateprofile',upload.single('profile_image'), verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');
    const obj = JSON.parse(req.body.main);
    if(req.file)
    {
    	var pf="images/"+req.file.filename;
    }
    else
    {
    	var pf=verify.data.profile_image;
    }

    let data = {
                    fname: obj.fname,
                    lname: obj.lname,
                    addressOne: obj.addressOne,
                    addressOneLatitude: obj.addressOneLatitude,
                    addressOneLongitude: obj.addressOneLongitude,
                    addressTwo: obj.addressTwo,
                    addressTwoLatitude: obj.addressTwoLatitude,
                    addressTwoLongitude: obj.addressTwoLongitude,
                    is_donor: obj.is_donor,
                    is_covid19: obj.is_covid19,
                    profile_image: pf
                };
    var id={user_id:verify.data.user_id};

    this.updateprofile(data,id, function(err, datas){
    	if(err)
    	{
    		res.json({"status":200,"message":err});
    	}
    	else
    	{
           res.json({"status":200,"message":"Data updated successfully"});
    	}
    });
});

function profile(id, callback) {
    var query = conn.query('select * from user WHERE ?', [id], function(err, result) {          
        callback(null, result);            
    });
}


app.get('/api/token', (req, res) => {

	var phone={mobile:req.body.mobile};

	profile(phone, function(err, result){
        if (isRes(result)) 
        {
            res.send({"status": 200,"message": "No result found"});
        } 
        else 
        {
            for (let key in result) 
            {
                let data = result[key];
                jwt.sign({data}, 'accesstoken', {expiresIn: '1440h'}, (err, token) => {
                       res.json({"status": 200,token});
                });
            }
        }
    });
});

function insert(tbl,data, callback) {
    var query = conn.query('INSERT into ?? SET ?', [tbl,data], function(err, result) {          
        callback(null, result);            
    });
}

function update_req_s(data,id,req,d_id, callback) {
    var query = conn.query('UPDATE req_status SET ? WHERE user_id = ? and request_id = ? and donor_id=?', [data,id,req,d_id], function(err, result) {          
        callback(null, result);            
    });
}

function already_req (id,d_id,req, callback) {
    var query = conn.query('select * from req_feedback where user_id= ? and requestor_id=? and request_id=?',[id,d_id,req], function(err, result) {           
        callback(null, result);            
    });
}

app.post('/api/give_rating_to_the_requestor', verifyToken, (req, res) => {
    var verify = jwt.verify(req.token, 'accesstoken');
    var data={user_id:verify.data.user_id,request_id:req.body.request_id,requestor_id:req.body.requestor_id,rating:req.body.rating,comment:req.body.comment};
        get_status(verify.data.user_id,req.body.requestor_id,req.body.request_id, function(err, status) {
            if(isRes(status))
            {
                 res.json({"status": 401,"message":"current request status is 3. Update donor status to 5 before giving feedback"});
            }
            else
            {
                already_req(verify.data.user_id,req.body.requestor_id,req.body.request_id, function(err, ndata){
                    if(isRes(ndata))
                    {
                        insert('req_feedback',data, function(err, data){
                            if(err)
                            {
                                res.json({"status": 401,"message":err});
                            }
                            else
                            {
                                var info={is_requestor_rating_done:1};
                                update_req_s(info,req.body.requestor_id,req.body.request_id,verify.data.user_id ,function(err, datas) {
                                	if(err)
                                	{
                                		res.json({"status": 401,"message":err});
                                	}
                                	else
                                	{
                                		res.json({"status": 200,"message":datas});
                                	}
                                });
                            }
                        }); 
                    }
                    else
                    {
                        res.json({"status": 401,"message":"Already given feedback"});
                    }    
                });
            } 
    });
})

function getdonationv2(id, callback) {
    var query = conn.query('SELECT r.*,U.fname,U.lname,R.status,U.profile_image,R.is_requestor_rating_done FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id LEFT JOIN user as U ON R.user_id=U.user_id where needbydatetime < now() and R.donor_id=' + id + ' and R.status=5',function(err, result) {           
        callback(null, result);            
    });
}

function getdonationupv2(id, callback) {
    var query = conn.query('SELECT R.*,"others" as "created_by",U.fname,U.lname,r.status,U.profile_image FROM req_status as r LEFT JOIN request as R ON r.request_id=R.request_id LEFT JOIN user as U ON r.user_id=U.user_id where r.user_id!=' + id + ' and R.needbydatetime > now() and r.donor_id=' + id + ' and (r.status=3 or r.status=1)',function(err, result) {           
        callback(null, result);            
    });
}


//get user count records
app.get('/api/getmydonationv2', verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');

        var data=[];
        getdonationv2(verify.data.user_id, function(err, result) {
            if(isRes(result))
            {
               data.push(); 
            }
            else
            {
               for (let key in result) 
               {
                let api = result[key];
                data.push(api);
               }
            }

        var zz=[];
        getdonationupv2(verify.data.user_id, function(err, result) {
            if(isRes(result))
            {
               zz.push(); 
            }
            else
            {
                for (let key in result) 
               {
                let data = result[key];
                zz.push(data);
               }
               
            }

                setTimeout(function() {
                if(isRes(data))
                {
                    var past=({"status":400,"message":"No data found"});
                }
                else
                {
                    var past=({"status":200,"result":data});
                }
                if(isRes(zz))
                {
                    var up=({"status":400,"message":"No data found"});
                }
                else
                {
                    var up=({"status":200,"result":zz});
                }

                res.json({"past":past,"upcoming":up});
            }, 30);
        });
   });
});

function getrequestv2(id, callback) {
    var query = conn.query('SELECT *,null as "accepter_data" FROM request where user_id = '+id+' and needbydatetime < now()',function(err, result) {           
        callback(null, result);            
    });
}
function getrequest_upv2(id, callback) {
    var query = conn.query('SELECT *,null as "accepter_data" FROM request where user_id = '+id+' and needbydatetime > now()',function(err, result) {           
        callback(null, result);            
    });
}

function user_status(data, callback) {
    var query = conn.query('SELECT * FROM req_status where donor_id = '+data.id+' and request_id!='+data.req+' and status = 3',function(err, result) {           
        callback(null, result);            
    });
}


function accepted(id, callback) {
    var query = conn.query('SELECT R.user_id,U.user_id,U.fname as accepter_fname,U.lname as accepter_lname,U.mobile,R.status,R.is_rating_done,R.rating,count(D.id) as donation_count,U.profile_image FROM req_status as R LEFT JOIN user as U ON R.donor_id=U.user_id LEFT JOIN donation_count as D ON R.donor_id=D.user_id  where R.request_id=' + conn.escape(id) + '  and  R.status!=2 and R.status!=4 GROUP BY  R.user_id,U.user_id,D.user_id,U.fname,U.lname,U.mobile,R.status,U.profile_image,R.is_rating_done,R.rating',function(err, result) {           
        callback(null, result);            
    });
}

function getpastreqv2(id,callback) {
    var query = conn.query('SELECT r.* FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id where R.request_id=' + conn.escape(id)+ ' and needbydatetime < now() GROUP BY id', function(err, pastreq) {           
        callback(null, pastreq);            
    });
}

function getupreqv2(id,callback) {
    var query = conn.query('SELECT r.* FROM request as r LEFT JOIN  req_status as R ON r.request_id=R.request_id where R.request_id=' + conn.escape(id)+ ' and needbydatetime > now() GROUP BY id', function(err, upreq) {           
        callback(null, upreq);            
    });
}


//get user count records
app.get('/api/getmyrequestv2', verifyToken, (req, res) => {
    var verify = jwt.verify(req.token, 'accesstoken');

    var data=[];
    getrequestv2(verify.data.user_id, function(err, result) {
            if(isRes(result)) 
            {
                data.push();
            } 
            else 
            {
                result.forEach(function(past) {
                    getpastreqv2(past.request_id, function(err, pastreq) {
                        if(isRes(pastreq))
                        {
                           data.push(past)
                        }
                        else
                        {
                            pastreq.forEach(function(result) { 
                                accepted(past.request_id, function(err, accepter_data) {
                                	if(isRes(accepter_data))
                                	{
                                        data.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": []});
                                	}
                                	else
                                	{
	                                	accepter_data.forEach(function(accept) { 
	                                		   var datas={id:accept.user_id,req:result.request_id};
	                                           user_status(datas, function(err, done) {
	                                           	if(!isRes(done))
	                                           	{
	                                                //data.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": []});
	                                           	}
	                                           	else
	                                           	{ 
	                                           		data.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": accepter_data});
	                                           	}
	                                        });
	                                	})
                                    }
                                })
                            });
                        }
                    });
                });
            }
        })
    var zz=[];
    getrequest_upv2(verify.data.user_id, function(err, results) {
            if(isRes(results)) 
            {
                zz.push();
            } 
            else 
            {
                results.forEach(function(pasts) {
                    getupreqv2(pasts.request_id, function(err, upreq) {

                        if(isRes(upreq))
                        {
                           zz.push(pasts)
                        }
                        else
                        {
                            upreq.forEach(function(result) { 
                                accepted(pasts.request_id, function(err, accepter_data) {
                                	if(isRes(accepter_data))
                                	{
                                        zz.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": []});
                                	}
                                	else
                                	{
	                                	accepter_data.forEach(function(accept) { 
	                                		   var datas={id:accept.user_id,req:result.request_id};
	                                           user_status(datas, function(err, done) {
	                                           	if(!isRes(done))
	                                           	{
	                                                zz.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": []});
	                                           	}
	                                           	else
	                                           	{
	                                           		zz.push({"id": result.id,"user_id": result.user_id,"request_id": result.request_id,"fname": result.fname,"lname": result.lname,"typeofdonation": result.typeofdonation,"bloodtype": result.bloodtype,"bloodrequestfor": result.bloodrequestfor,"numberofunits": result.numberofunits,"addressofdonation": result.addressofdonation,"donationlatitude": result.donationlatitude,"donationlongitude": result.donationlongitude,"needbydatetime": result.needbydatetime,"mobile": result.mobile,"alternatenumber": result.alternatenumber,"isnewsfeedvisible": result.isnewsfeedvisible,"distance": result.dist,"created_by": "others","created_at": result.created_at,"profile_image": result.profile_image,"status": result.status,"accepter_data": accepter_data});
	                                           	}
	                                        });
	                                	});
                                    }
                                });
                            });
                        }
                    });
                });
            }
        })

    setTimeout(function() {
        if(isRes(data))
        {
            var past=({"status":400,"message":"No data found"});
        }
        else
        {
            var past=({"status":200,"result":data});
        }
        if(isRes(zz))
        {
            var up=({"status":400,"message":"No data found"});
        }
        else
        {
            var up=({"status":200,"result":zz});
        }

        res.json({"past":past,"upcoming":up});
    }, 30);
});

//update device id
app.post('/api/update_device_id', verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');

    let data =  {
                    token: req.body.token,
                    device_type: req.body.device_type                    
                };
    var id={user_id:verify.data.user_id};

    this.updateprofile(data,id, function(err, datas){
    	if(err)
    	{
    		res.json({"status":200,"message":err});
    	}
    	else
    	{
           res.json({"status":200,"message":"Data updated successfully"});
    	}
    });
});	

function expired_req()
{
	let sql = 'SELECT * FROM request where  needbydatetime > (NOW() - INTERVAL 10 MINUTE) and needbydatetime < NOW()';
    let query = conn.query(sql, (err, result) => {
        if (isRes(result)) 
        {
            console.log('no data found');
        } 
        else 
        {
        	result.forEach(function(main) {
	            get_token(main.user_id,function(err, response) {
				    if(response[0].token===null)
					{
						console.log('Token not found');
					}
					else
					{
						var data={to:response[0].token,title:"Donation expired",body:"How was your blood request experience."};
	                    notification(data);
					}
				});
	        });
        }
    });
}

function confirm(id, callback) {
    var query = conn.query('SELECT donor_id FROM req_status where request_id=' + id + ' and status=3',function(err, result) {           
        callback(null, result);            
    });
}

function going_to_expire(time)
{
    let sql = 'SELECT * FROM request where  NOW() = (needbydatetime - INTERVAL '+time+' HOUR)';
    let query = conn.query(sql, (err, result) => {
        if (isRes(result)) 
        {
            
        } 
        else 
        {
            result.forEach(function(main) {
                get_token(main.user_id,function(err, response) {
                    if(response[0].token===null)
                    {

                    }
                    else
                    {
                        var data={to:response[0].token,title:"Request Status",body:"The donation for requested blood will start in next "+time+" hr at "+main.addressofdonation};
                        notification(data);
                    }
                });
                confirm(main.request_id,function(err, conf) {
                    if(isRes(conf))
                    {
                       
                    }
                    else
                    {
                        conf.forEach(function(hours) {
                            get_token(hours.donor_id,function(err, users) {
                                users.forEach(function(make) {
                                    var data={to:make.token,title:"Request Status",body:"You have a donation in next "+time+" hrs and "+main.addressofdonation};
                                    notification(data);
                                });
                            });
                        });
                    }
                });
            });
        }
    });
}

//cron job push notifications check last 10 min expired requests
cron.schedule("*/10 * * * *", function() {
    expired_req();
});



cron.schedule("*/1 * * * * *", function() {
    going_to_expire(24);
    going_to_expire(1);
});



function req_users(id, callback) {
    var query = conn.query('SELECT user_id FROM req_status where request_id = '+id+' and (status=1 or status =3)',function(err, result) {           
        callback(null, result);            
    });
}

//update request
app.post('/api/update_req', verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');

    let data =  {
                    typeofdonation: req.body.typeofdonation,
                    bloodrequestfor: req.body.bloodrequestfor,
                    numberofunits: req.body.numberofunits,
                    addressofdonation: req.body.addressofdonation,
                    donationlatitude: req.body.donationlatitude,
                    donationlongitude: req.body.donationlongitude,
                    needbydatetime: req.body.needbydatetime,
                    mobile: req.body.mobile,
                    alternatenumber: req.body.alternatenumber,
                    isnewsfeedvisible: req.body.isnewsfeedvisible                    
                };

    request_exist(req.body.request_id, function(err, exist) {
        if(isRes(exist)) 
        {
            res.json({"status":401,"message":"Request_id doesnot exist in the database"});
        }
        else 
        {
            if(data.numberofunits<exist[0].numberofunits) 
            {
                if(exist[0].confirmed<exist[0].numberofunits) 
                {
                    update('request',data,{request_id:req.body.request_id}, function(err, datas) {
                        res.json({"status":200,"message":"Data updated successfully"});
                        req_users(req.body.request_id, function(err, exist) {
                        	if(isRes(exist))
                        	{
                        		exist.forEach(function(main) {
	                        		get_token(main.user_id,function(err, response) {
									    if(response[0].token===null)
										{
											console.log('Token not found');
										}
										else
										{
											var data={to:response[0].token,title:"Request updated",body:"Please check updated request details."};
						                    notification(data);
						                    console.log(data);
										}
									});
                        	    });
                        	}
                        });
                    });
                } 
                else 
                {
                    res.json({"status":402,"message":"To decrease number of required units, you need to decline some donors"});
                }
            } 
            else 
            {
                update('request',data,{request_id:req.body.request_id,}, function(err, datas) {
                    res.json({"status":200,"message":"Data updated successfully"});
                    req_users(req.body.request_id, function(err, exist) {
                    	if(isRes(exist))
                    	{
                    		exist.forEach(function(main) {
                        		get_token(main.user_id,function(err, response) {
								    if(response[0].token===null)
									{
										console.log('Token not found');
									}
									else
									{
										var data={to:response[0].token,title:"Request updated",body:"Please check updated request details."};
					                    notification(data);
					                    console.log(data)
									}
								});
                    	    });
                    	}
                    });
                });
            }
        }
    });
}); 

function deletes(id, callback) {
    var query = conn.query('DELETE FROM ??',[id],function(err, result) {           
        callback(null, result);            
    });
}

//update request
app.post('/api/delete', verifyToken, (req, res) => {

    var verify = jwt.verify(req.token, 'accesstoken');

    deletes(req.body.table, function(err, datas) {
        if(err)
        {
         	res.json({"status":400,"message":err});
        }
        else
        {
         	res.json({"status":400,"message":"Successfully deleted table data"});
        }
    });
}); 

//Server listening 
app.listen(3000, console.log('connected'));